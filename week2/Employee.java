import java.util.Scanner;
class Employee
{
    private String First_Name;
    private String Last_Name;
    private double Salary;

    Employee()
    {
        First_Name="";
        Last_Name="";
        Salary=0.0;
    }

    Employee(String fn, String ln, double s)
    {
        First_Name = fn;
        Last_Name = ln;
        Salary = s;
        if(s<0)
        	Salary = 0;
    }
    public void setFN(String boyz)
    {
        First_Name = boyz;
    }

    public void setLN(String boyz)
    {
        Last_Name = boyz;
    }
    public void setS(double Salary)
    {
        this.Salary = Salary;
        if(Salary<0)
        	this.Salary=0;
    }
    public double getS()
    {
        return Salary;
    }
    public double getSY()
    {
        return Salary*12;
    }
    public String getFN()
    {
        return First_Name;
    }
    public String getLN()
    {
        return Last_Name;
    }
    public void SalaryGain()
    {
        this.Salary = this.Salary*1.1;
    }
}   

